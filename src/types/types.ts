import {AUTHORIZATION_FAILURE, AUTHORIZATION_REQUEST, AUTHORIZATION_SUCCESS} from "../redux/authReducer";

export interface ILoginFormData {
    email: string
    password: string
}

export interface IUserData {
    email: string
    isAuth: boolean
}

export interface LoginFailurePayload {
    errorMessage: string;
}

export interface AuthorizationRequest {
    type: typeof AUTHORIZATION_REQUEST;
    payload: ILoginFormData;
}

export type AuthorizationSuccess = {
    type: typeof AUTHORIZATION_SUCCESS;
    payload: IUserData;
};

export type AuthorizationFailure = {
    type: typeof AUTHORIZATION_FAILURE;
    payload: LoginFailurePayload;
};

export type AuthActions =
    | AuthorizationRequest
    | AuthorizationSuccess
    | AuthorizationFailure;