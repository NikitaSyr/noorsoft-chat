import React from 'react';
import './App.css';
import Login from "./components/Login/Login";

const App = () => {
  return (
    <div className="app">
        <div className="app__wrapper">
            <div className="content">
                {/*<header className="app__header">*/}
                {/*</header>*/}
                <div className="content__load">
                    <Login/>
                </div>
            </div>
        </div>

    </div>
  );
}

export default App;
