import {signInWithEmailAndPassword} from "firebase/auth";
import {auth} from "./firebase";
import firebase from "firebase/compat";
import {User} from '@firebase/auth-types'



export function* requestMessages() {
}

export const authAPI = {
    async signIn(email: string, password: string): Promise<User> {
        try {
            const {user} = await signInWithEmailAndPassword(auth, email, password);
            return user as User;
        }
        catch (e: any) {
            return e;
        }
    }
}