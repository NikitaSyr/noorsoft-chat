import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyDnU11DLNmKbFR9UaWqcwiKHZ-Fsx9Wotc",
    authDomain: "noorsoft-chat1.firebaseapp.com",
    projectId: "noorsoft-chat1",
    storageBucket: "noorsoft-chat1.appspot.com",
    messagingSenderId: "341256140634",
    appId: "1:341256140634:web:2065a097927dbed204a563",
    measurementId: "G-YYM1TNJZDG"
};

export const firebase_app= initializeApp(firebaseConfig);
export const auth = getAuth(firebase_app);

