export const fireBaseErrorSlicer = (errorMessage: string) => {
   return errorMessage.slice(10, errorMessage.length)
}