import {
    AuthorizationFailure,
    AuthorizationRequest,
    AuthorizationSuccess,
    ILoginFormData,
    IUserData, LoginFailurePayload
} from "../types/types";
import {AppState} from "./reduxStore";

// export const MESSAGES_REQUEST = 'noorsoft-chat/messages/MESSAGES_REQUEST';
// export const MESSAGES_SUCCESS = 'noorsoft-chat/messages/MESSAGES_SUCCESS';
// export const MESSAGES_FAILURE = 'noorsoft-chat/messages/MESSAGES_FAILURE';

export const AUTHORIZATION_REQUEST = 'AUTHORIZATION_REQUEST';
export const AUTHORIZATION_SUCCESS = 'AUTHORIZATION_SUCCESS';
export const AUTHORIZATION_FAILURE = 'AUTHORIZATION_FAILURE';

interface State {
    email: string | null,
    password: string | null,
    isAuth: boolean,
    errorMessage: string,
}

const initialState: State = {
    email: null,
    password: null,
    isAuth: false,
    errorMessage: "",
}

export const authReducer = (state = initialState, action: any): State => {
    switch (action.type) {
        case AUTHORIZATION_REQUEST: {
            return {
                ...state,
                ...action.payload
            }
        }
        case AUTHORIZATION_SUCCESS: {
            return {
                ...state,
                password: null,
                errorMessage: "",
                ...action.payload
            }
        }
        case AUTHORIZATION_FAILURE: {
            return {
                ...state,
                password: null,
                errorMessage: action.payload,
            }
        }
        default:
            return {...state};
    }
}


export const actions = {
    requestAuthUserDataAC: (payload: ILoginFormData): AuthorizationRequest => ({
        type: AUTHORIZATION_REQUEST,
        payload
    }),
    setAuthUserDataAC: (payload: IUserData): AuthorizationSuccess => ({
        type: AUTHORIZATION_SUCCESS,
        payload
    }),
    setErrorMessageAC: (payload: LoginFailurePayload): AuthorizationFailure => ({
        type: AUTHORIZATION_FAILURE,
        payload
    })
}


export const getAuthorizationPassword = (state: AppState): string | null => {
    return state.loginPage.password
}

export const getUserEmail = (state: AppState): string | null => {
    return state.loginPage.email
}

export const getIsUserAuth = (state: AppState): boolean => {
    return state.loginPage.isAuth
}

export const getErrorMessage = (state: AppState): string => {
    return state.loginPage.errorMessage
}
