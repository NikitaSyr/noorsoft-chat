import { createStore, applyMiddleware,combineReducers } from "redux";
import createSagaMiddleware from "redux-saga";
import {authReducer} from "./authReducer";
import rootSaga from "./sagas/index";

const rootReducer = combineReducers({
    loginPage: authReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof store.dispatch

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export const action = (type: AppDispatch) => store.dispatch({type})

export default store;