import {all, fork} from 'redux-saga/effects'
import {requestMessages} from "../../api/api";
import signInSaga from "./authSaga";

// export function* watchLastRequestMessages() {
//     yield takeLatest('MESSAGES_REQUEST', requestMessages)
// }

export default function* rootSaga() {
    yield all([
        fork(signInSaga)
    ])
}