import {call, put, select, SagaReturnType, takeLatest} from "redux-saga/effects";
import {actions, AUTHORIZATION_REQUEST, getAuthorizationPassword, getUserEmail} from "../authReducer";
import {authAPI} from "../../api/api";
import {TakeableChannel} from "redux-saga";
import {fireBaseErrorSlicer} from "../../util/helpers";

type LoginServiceResponse = SagaReturnType<typeof authAPI.signIn>

function* signInRequestSaga() {
    const password: string | null = yield select(getAuthorizationPassword);
    const email: string | null = yield select(getUserEmail);
    if (password && email) {
        try {
            const response: LoginServiceResponse = yield call(authAPI.signIn, email, password);
            if (response.email) {
                yield put(
                    actions.setAuthUserDataAC({email: response.email, isAuth: true})
                );
            } else {
                yield put(
                    //TODO
                    // Fix types or error catch

                    // @ts-ignore
                    actions.setErrorMessageAC(fireBaseErrorSlicer(response.message))
                );
            }
        } catch (e: any) {
            yield put(
                actions.setErrorMessageAC(e.message)
            );
        }
    }
}

function* signInSaga() {
    yield takeLatest(AUTHORIZATION_REQUEST as unknown as TakeableChannel<unknown>, signInRequestSaga);
}

export default signInSaga;