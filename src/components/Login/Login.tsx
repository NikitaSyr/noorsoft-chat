import React from 'react';
import s from './Login.module.css'
import LoginForm from "./LoginForm/LoginForm";
import {useDispatch, useSelector} from "react-redux";
import {ILoginFormData} from "../../types/types";
import {actions, getErrorMessage} from "../../redux/authReducer";

const Login = () => {
    const dispatch = useDispatch();
    const errorMessage = useSelector(getErrorMessage)
    const loginFormSubmit = (formData: ILoginFormData) => {
        dispatch(actions.requestAuthUserDataAC(formData))
    }
    return (
        <div className={s.login}>
            <LoginForm loginFormSubmit={loginFormSubmit}/>
            <div>{errorMessage}</div>
        </div>
    )
}

export default Login;