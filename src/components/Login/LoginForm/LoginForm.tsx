import React, {FC} from 'react';
import {useFormik} from 'formik';
import s from './LoginForm.module.css'
import {ILoginFormData} from "../../../types/types";

type PropsType = {
    loginFormSubmit: (formData: ILoginFormData) => void
}

const LoginForm: FC<PropsType> = ({loginFormSubmit}) => {
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        onSubmit: values => {
            loginFormSubmit(values);
        },
    });
    return (
        <form onSubmit={formik.handleSubmit} className={s.login__form}>
            <label htmlFor="email" className={s.form__item}>Email</label>
            <input
                id="email"
                name="email"
                type="email"
                required={true}
                onChange={formik.handleChange}
                value={formik.values.email}
                className={s.form__item}
            />
            <label htmlFor="password" className={s.form__item}>Password</label>
            <input
                id="password"
                name="password"
                type="password"
                required={true}
                onChange={formik.handleChange}
                value={formik.values.password}
                className={s.form__item}
            />
            <button type="submit" className={s.form__item}>Submit</button>
        </form>
    );
};

export default LoginForm;