import { render } from '@testing-library/react';
import App from "../../App";
import {Provider} from "react-redux";
import store from "../../redux/reduxStore";

test('renders the landing page', () => {
    render(<Provider store={store}>
        <App />
    </Provider>);
});